/*
Name: Blink
Description: Turns on an LED for one second then turns it off for one second.
Author: James Ford <james.t.ford@gmail.com>

Notes: Assumes LED is attached to pin 2, see sketch file.

*/

// Sets up an array to hold the pin numbers that the LEDs are attached to
// remember that we have LEDs connected to PINs 2 through 9
int leds [] = {2,3,4,5,6,7,8,9};

// the setup routine runs once when you press reset
void setup () {
  // loop through all of the ditinal pins
  // since an array starts from 0 and we have 8 pins we will loop from 0 through 7
  for (int i=0; i<=7; i++) {
    // initialize the digital pin as an output
     pinMode(leds[i], OUTPUT);
  }
}

// the loop routine runs over and over again
void loop () {
   for (int i=0; i<=7; i++) {
      digitalWrite(leds[i], HIGH);                // turn the LED on by making the voltage HIGH
   }
   delay(1000);                                   // wait for a second
   for (int i=0; i<=7; i++) {
      digitalWrite(leds[i], LOW);                 // turn the LED off by making the voltage LOW
   }
   delay(1000);                                   // wait for a second
}
