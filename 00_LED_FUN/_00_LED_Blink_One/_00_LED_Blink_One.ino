/*
Name: Blink
Description: Turns on an LED for one second then turns it off for one second.
Author: James Ford <james.t.ford@gmail.com>

Notes: Assumes LED is attached to pin 2, see sketch file.

*/

// Pin 2 has an LED connected to it so we give it a name of led
int led = 2;

// the setup routine runs once when you press reset
void setup () {
  // initialize the ditigal pin as an output
  pinMode(led, OUTPUT);
}

// the loop routine runs over and over again
void loop () {
   digitalWrite(led, HIGH);     // turn the LED on by making the voltage HIGH
   delay(1000);                 // wait for a second
   digitalWrite(led, LOW);      // turn the LED off by making the voltage LOW
   delay(1000);                 // wait for a second
}
