![Fritzing Sketch](https://bitbucket.org/jamestford/arduino/raw/ba08a62e40e00153b905f44c5a7d8144a0bc89be/00_LED_FUN/images/fritzingsketch.png)

## Fritzing Sketch

We have included a Fritzing sketch which can be opened by the [Fritzing](http://fritzing.org/download)

## LED positive versus negative

LED's have a positive and negative side, sometimes the LED will have a label but other times you will only be able to tell by the length of the wires, the longer wire will always be positive and the shorter will be negative. I remember this by thinking to myself electricity wants the shortest path to ground, so the short wire represents the negative side.
LED's also require a resistor to lower the power being pushed through them, most LED's operate at 2 volts with 20mA. 
The LED's we are using in this sketch are 5mm RED 20mA LED's which has a typical forward voltage of 2.0 volts. 

## Calculating LED resistor value

LED's must have a resistor connected to limit the current through the LED, otherwise it will burn out. 
The resistor value, R is given by: R = (VS - VL) / I where

* VS = supply voltage 
* VL = LED voltage (usually 2V) 
* I = LED current (e.g. 20mA = 0.02A) 

Make sure the LED current you choose is less than the maximum permitted and convert the current to amps (A) so the calculation will give the resistor value in ohms (ohm). 
To convert mA to A divide the current in mA by 1000 because 1mA = 0.001A. If the calculated value is not available choose the nearest standard resistor value which is greater, so that the current will be a little less than you chose. In fact you may wish to choose a greater resistor value to reduce the current (to increase battery life for example) but this will make the LED less bright.

For example

If the supply voltage VS = 5V, and you have a red LED (VL = 2V), requiring a current I = 20mA = 0.020A, 
R = (5V - 2V) / 0.02A = 150ohm. If we wanted to be a little safer we could use 18mA instead which would give us a resistor size of 180ohms.

Check out this website at [http://led.linear1.org/1led.wiz](http://led.linear1.org/1led.wiz) which will automate the conversion for you. You will need to input the source voltage, forward voltage, and forward current. Use the following:

* voltage = 5v from arduino
* forward voltage = 2v based on LED datasheet
* forward current = 20ma based on LED datasheet